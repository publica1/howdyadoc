FROM ubuntu:22.10
# RUN apt-get update
# RUN apt-get install -y libreoffice-writer ruby-mustache python3-cson git make
# RUN apt-get update && apt-get install -y apt-utils libreoffice-writer ruby-mustache python3-cson git make
RUN apt-get update && apt-get install -y libreoffice-writer ruby-mustache python3-cson git make


RUN git clone https://gitlab.com/howdyadoc/toolchain.git tmp
RUN cp tmp/release/howdyadoc_1.0-1_amd64.deb tmp
RUN apt-get install -y /tmp/howdyadoc_1.0-1_amd64.deb

#RUN apt-get install -y ssh
#RUN mkdir -p -m 0700 ~/.ssh && ssh-keyscan gitlab.com >> ~/.ssh/known_hosts
#RUN ssh-keygen -q -t rsa -N '' -f /id_rsa



ENTRYPOINT ["ls", "home", "-l"]
